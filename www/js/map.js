var map;
var infoWindow;
var i = 0;
var markersData = [];
var markers = [];
var circle = [];


function initMap() {
   // função para pegar as coordenadas de latitude e longitude do GPS do celular ou navegador
   navigator.geolocation.getCurrentPosition(function(position){
   // função que vai atualizar a as coordenadas do usuário e retornar os usuários no raio configurado
   updateCoordinate(position.coords.latitude, position.coords.longitude);
   });

   var mapOptions = {
    disableDefaultUI: true
  }
   // cria o mapa
   map = new google.maps.Map(document.getElementById('map'), mapOptions);
   // cria a nova Info Window com referência à variável infowindow
   // o conteúdo da Info Window será atribuído mais tarde
   infoWindow = new google.maps.InfoWindow();

   // evento que fecha a infoWindow com click no mapa
   google.maps.event.addListener(map, 'click', function() {
      infoWindow.close();
   });

   // Chamada para a função que vai percorrer a informação
   // contida na variável markersData e criar os marcadores a mostrar no mapa

}

// Esta função vai percorrer a informação contida na variável markersData
// e cria os marcadores através da função createMarker
function displayMarkers(){

   // esta variável vai definir a área de mapa a abranger e o nível do zoom
   // de acordo com as posições dos marcadores
   var bounds = new google.maps.LatLngBounds();
   // Loop que vai estruturar a informação contida em markersData
   // para que a função createMarker possa criar os marcadores
   for (var i = 0; i < markersData.length; i++){
      var latlng = new google.maps.LatLng(markersData[i].latitude, markersData[i].longitude);
      var id = markersData[i].id;
      var nome = markersData[i].full_name;
      var gender = markersData[i].gender;
      var email = markersData[i].email;
      var range = parseInt(markersData[i].range);
      var photo = markersData[i].photo;
      var type = markersData[i].type;
      var username = markersData[i].username;
      var promo = markersData[i].promo;
      createMarker(id, latlng, nome, gender, email, range, i, photo, type, username, promo);
      // Os valores de latitude e longitude do marcador são adicionados à
      // variável bounds
      bounds.extend(latlng);
   }

   // Depois de criados todos os marcadores
   // a API através da sua função fitBounds vai redefinir o nível do zoom
   // e consequentemente a área do mapa abrangida.
   map.fitBounds(bounds);
}

// Função que cria os marcadores e define o conteúdo de cada Info Window.
function createMarker(id, latlng, nome, gender, email, range, pos, photo, type, username, promo){

   var marker;
   if(pos == 0){
      if(type == "user"){
         var pinIcon = new google.maps.MarkerImage(
          'http://www.bump.vivainovacao.com/public/'+photo+'',
          null, /* size is determined at runtime */
          null, /* origin is 0,0 */
          null, /* anchor is bottom center of the scaled image */
          new google.maps.Size(50, 40)
         );

          marker = new google.maps.Marker({
            map: map,
            position: latlng,
            title: nome,
            range: range
         });
         marker.setIcon(pinIcon);
         if(circle.length == 0){
            var rangeCircle = new google.maps.Circle({
               strokeColor: '#FF0000',
               strokeOpacity: 0.8,
               strokeWeight: 2,
               fillColor: '#FF0000',
               fillOpacity: 0.35,
               map: map,
               center: marker.position,
               radius: marker.range * 1000
            });
            circle.push(rangeCircle);
         }
      }else{
         var pinIcon = new google.maps.MarkerImage(
          'http://www.bump.vivainovacao.com/public/'+photo+'',
          null, /* size is determined at runtime */
          null, /* origin is 0,0 */
          null, /* anchor is bottom center of the scaled image */
          new google.maps.Size(50, 40)
         );

          marker = new google.maps.Marker({
            map: map,
            position: latlng,
            title: nome,
            range: range,
            //icon: '../img/icons/iconBusiness.png'
         });
          marker.setIcon(pinIcon);
         if(circle.length == 0){
            var rangeCircle = new google.maps.Circle({
                  strokeColor: '#FF0000',
                  strokeOpacity: 0.8,
                  strokeWeight: 2,
                  fillColor: '#FF0000',
                  fillOpacity: 0.35,
                  map: map,
                  center: marker.position,
                  radius: marker.range * 1000
             });
            circle.push(rangeCircle);
        }
     }
   }else{
      if(type == "user"){
         var pinIcon = new google.maps.MarkerImage(
          'http://www.bump.vivainovacao.com/public/'+photo+'',
          null, /* size is determined at runtime */
          null, /* origin is 0,0 */
          null, /* anchor is bottom center of the scaled image */
          new google.maps.Size(50, 40)
         );

         marker = new google.maps.Marker({
         map: map,
         position: latlng,
         title: nome,
         range: range,
         //icon: '../img/icons/iconYou.png'
      });
         marker.setIcon(pinIcon);
    }else{
      var pinIcon = new google.maps.MarkerImage(
          'http://www.bump.vivainovacao.com/public/'+photo+'',
          null, /* size is determined at runtime */
          null, /* origin is 0,0 */
          null, /* anchor is bottom center of the scaled image */
          new google.maps.Size(50, 40)
         );

        marker = new google.maps.Marker({
         map: map,
         position: latlng,
         title: nome,
         range: range,
         //icon: '../img/icons/iconBusiness.png'
      });
        marker.setIcon(pinIcon);
    }

   }
   markers.push(marker);

 if(type == "user" && pos != 0){
      // Evento que dá instrução à API para estar alerta ao click no marcador.
   // Define o conteúdo e abre a Info Window.
   google.maps.event.addListener(marker, 'click', function() {

      // Variável que define a estrutura do HTML a inserir na Info Window.
      var iwContent = '<div id="iw_container">' +
            '<div class="w-col w-col-2 w-col-small-2 w-col-tiny-2e"><img src="http://www.bump.vivainovacao.com/public/'+photo+'" width="45px;" height="45px;"></div>' +
         '<div class="w-col w-col-10 w-col-small-10 w-col-tiny-10" style="margin-top: 5px;"> <p>Username: <strong>'+username+'</strong> </p>' +
         '<p>Gender: <strong>'+gender+'</strong></p>' +
         '<p><a href="javascript:blockUser('+id+')" style="background-color: red; color:white; border-radius: 5px; text-decoration: none; padding: 3px 4px;"> Block user </a></p></div></div>';

      // O conteúdo da variável iwContent é inserido na Info Window.
      infoWindow.setContent(iwContent);

      // A Info Window é aberta.
      infoWindow.open(map, marker);
   });
}
else if(type == "user" && pos == 0){
      // Evento que dá instrução à API para estar alerta ao click no marcador.
   // Define o conteúdo e abre a Info Window.
   google.maps.event.addListener(marker, 'click', function() {

      // Variável que define a estrutura do HTML a inserir na Info Window.
      var iwContent = '<div id="iw_container">' +
            '<div class="w-col w-col-2 w-col-small-2 w-col-tiny-2e"><img src="http://www.bump.vivainovacao.com/public/'+photo+'" width="45px;" height="45px;"></div>' +
         '<div class="w-col w-col-10 w-col-small-10 w-col-tiny-10" style="margin-top: 5px; margin-bottom: 5px;"> <p>Username: <strong>'+username+'</strong></p>' +
         '<p>Gender: <strong>'+gender+'</strong></p>' +
         '</div></div>';

      // O conteúdo da variável iwContent é inserido na Info Window.
      infoWindow.setContent(iwContent);

      // A Info Window é aberta.
      infoWindow.open(map, marker);
   });
}else if(type == "business" && pos == 0){
      // Evento que dá instrução à API para estar alerta ao click no marcador.
   // Define o conteúdo e abre a Info Window.
   google.maps.event.addListener(marker, 'click', function() {

      // Variável que define a estrutura do HTML a inserir na Info Window.
      var iwContent = '<div id="iw_container">' +
            '<div class="w-col w-col-2 w-col-small-2 w-col-tiny-2e"><img src="http://www.bump.vivainovacao.com/public/'+photo+'" width="45px;" height="45px;"></div>' +
         '<div class="w-col w-col-10 w-col-small-10 w-col-tiny-10" style="margin-top: 5px;"> <p>Company: <strong>'+nome+'</strong> </p>' +
         '</div></div>';

      // O conteúdo da variável iwContent é inserido na Info Window.
      infoWindow.setContent(iwContent);

      // A Info Window é aberta.
      infoWindow.open(map, marker);
   });
}else if(type == "business" && pos != 0){
      // Evento que dá instrução à API para estar alerta ao click no marcador.
   // Define o conteúdo e abre a Info Window.
   google.maps.event.addListener(marker, 'click', function() {

      // Variável que define a estrutura do HTML a inserir na Info Window.
      var iwContent = '<div id="iw_container">' +
            '<div class="w-col w-col-2 w-col-small-2 w-col-tiny-2e"><img src="http://www.bump.vivainovacao.com/public/'+photo+'" width="45px;" height="45px;"></div>' +
         '<div class="w-col w-col-10 w-col-small-10 w-col-tiny-10" style="margin-top: 5px;"><p>Company: <strong>'+nome+'</strong> </p>' +
         '<p><a href="javascript:blockUser('+id+')" style="background-color: red; color:white; border-radius: 5px; text-decoration: none; padding: 3px 4px;"> Block user </a></p>';
         if(promo=="ok"){
            iwContent += '<p><a href="#" onClick="listPromo('+id+')" style="background-color: blue; color:white; border-radius: 5px; text-decoration: none; padding: 3px 4px;">Promotions</a></p></div></div>';
         } else {
            iwContent += '</div></div>';
         }
         
      // O conteúdo da variável iwContent é inserido na Info Window.
      infoWindow.setContent(iwContent);

      // A Info Window é aberta.
      infoWindow.open(map, marker);
   });
}
}

function removeComponentes(){

      for (var i = 0; i < markers.length; i++) {
         markers[i].setMap(null);
      }
      for (var i = 0; i < circle.length; i++) {
        // circle[i].setMap(null);
        circle[i].setCenter(markers[0].position);
      }
      navigator.geolocation.getCurrentPosition(function(position){
      updateCoordinate(position.coords.latitude, position.coords.longitude);
      });
}

function getUsersByRadius(latitude, longitude) {
         var parameters = {
            id: window.localStorage.getItem("id"),
            group: window.localStorage.getItem("group_id"),
         }
        $.ajax({
            type: "POST",
            data: parameters,
            url: "http://www.bump.vivainovacao.com/UserNormal/GetUsersByCoordinates",
            dataType: 'JSON',
            async: false,
            success:function(data){
               markersData = data;
               console.log(data);
               displayMarkers();
            }
        });
        return true;
}

function updateCoordinate(latitude, longitude){
      var parameters = {
         id: window.localStorage.getItem("id"),
         latitude: latitude,
         longitude: longitude
      }
     $.ajax({
         type: "POST",
         data: parameters,
         url: "http://www.bump.vivainovacao.com/UserNormal/UpdateCoordinates",
         dataType: 'JSON',
         async: false,
         success:function(data){
             getUsersByRadius(latitude, longitude);
         }
     });
}
function blockUser(id) {
         var parameters = {
            id: id,
            usergroups_user_id: window.localStorage.getItem("id")
         }
        $.ajax({
            type: "POST",
            data: parameters,
            url: "http://www.bump.vivainovacao.com/UserBlocked/create",
            dataType: 'JSON',
            async: false,
            success:function(data){
               if (data.retorno == 'ok')
                  removeComponentes();
               if (data.retorno == 'not')
                  console.log('error');
            }
        });
}
function listPromo(id) {
   window.localStorage.setItem("id_company", id);
   $(location).attr('href','promotions_list_business.html');
}

